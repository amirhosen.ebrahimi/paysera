package com.paySera.supperApp

import com.paySera.supperApp.core.CoreComponent
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import com.paySera.supperApp.di.annotation.AppScope
import com.paySera.supperApp.di.module.ActivityBindingModule
import com.paySera.supperApp.di.module.WorkerBindingModule


@AppScope
@Component(
    modules = [AndroidInjectionModule::class, ActivityBindingModule::class, WorkerBindingModule::class],
    dependencies = [CoreComponent::class]
)
interface AppComponent : AndroidInjector<Application> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun coreComponent(coreComponent: CoreComponent): Builder
        fun build(): AppComponent
    }

}
