package com.paySera.supperApp


import com.facebook.stetho.Stetho
import com.paySera.supperApp.core.CoreComponent
import com.paySera.supperApp.core.DaggerCoreComponent
import com.paySera.supperApp.core.di.modules.ContextModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import timber.log.Timber
import androidx.work.Configuration
import androidx.work.WorkManager
import com.paySera.supperApp.tasks.core.WorkerFactory
import javax.inject.Inject

class Application : DaggerApplication() {
    private lateinit var coreComponent: CoreComponent

    @Inject
    lateinit var workerFactory: WorkerFactory

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
        // use our custom factory so that work manager will use it to create our worker
        val workManagerConfig = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
        WorkManager.initialize(this, workManagerConfig)
    }


    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .application(this)
            .coreComponent(getCoreComponent())
            .build()
    }

    private fun getCoreComponent(): CoreComponent {
        if (!this::coreComponent.isInitialized) {
            coreComponent = DaggerCoreComponent
                .builder()
                .contextModule(ContextModule(this))
                .build()
        }

        return coreComponent
    }

}