package com.paySera.supperApp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.paySera.supperApp.MainActivity
import com.paySera.supperApp.data.di.ModuleRepositoryPaySera
import com.paySera.supperApp.di.annotation.ActivityScoped
import com.paySera.supperApp.placeList.PlaceListScope
import com.paySera.supperApp.placeList.presentation.CurrencyConverterFragment
import com.paySera.supperApp.placeList.presentation.di.PlaceListAssistedModule
import com.paySera.supperApp.placeList.presentation.di.PlaceListPresentationModule
import com.paySera.supperApp.tasks.updateCurrencyRates.UpdateCurrencyRates
import com.paySera.supperApp.tasks.updateCurrencyRates.presentation.UpdateCurrencyRatesWorker


@Module
abstract class WorkerBindingModule {

    @UpdateCurrencyRates
    @ContributesAndroidInjector(modules = [ModuleRepositoryPaySera::class])
    internal abstract fun bindUpdateCurrencyRatesWorker(): UpdateCurrencyRatesWorker

}