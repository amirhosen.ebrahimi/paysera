package com.paySera.supperApp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.paySera.supperApp.MainActivity
import com.paySera.supperApp.di.annotation.ActivityScoped


@Module
abstract class ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [MainActivityModule::class, FragmentBindingModule::class])
    internal abstract fun mainActivity(): MainActivity
}