package com.paySera.supperApp.di.module


import com.paySera.supperApp.placeList.PlaceListScope
import com.paySera.supperApp.placeList.presentation.CurrencyConverterFragment
import com.paySera.supperApp.placeList.presentation.di.PlaceListAssistedModule
import com.paySera.supperApp.placeList.presentation.di.PlaceListPresentationModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {


    @PlaceListScope
    @ContributesAndroidInjector(modules = [PlaceListAssistedModule::class, PlaceListPresentationModule::class  ])
    internal abstract fun bindCurrencyConverterFragment(): CurrencyConverterFragment

}