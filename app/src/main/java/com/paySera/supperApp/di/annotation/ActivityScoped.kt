package com.paySera.supperApp.di.annotation

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class ActivityScoped


