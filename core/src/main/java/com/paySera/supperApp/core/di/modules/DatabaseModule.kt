package com.paySera.supperApp.core.di.modules

import android.content.Context
import androidx.room.Room
import com.paySera.supperApp.core.BuildConfig
import dagger.Module
import dagger.Provides
import com.paySera.supperApp.common.di.qualifier.ApplicationContext
import com.paySera.supperApp.data.db.PaySeraDataBase
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) = Room.databaseBuilder(
        context,
        PaySeraDataBase::class.java,
        BuildConfig.DATABASE_NAME
    ).build()

    @Provides
    @Singleton
    fun provideCurrencyRateDao(appDatabase: PaySeraDataBase) =
        appDatabase.getCurrencyRateDao()

    @Provides
    @Singleton
    fun provideBalancesDao(appDatabase: PaySeraDataBase) =
        appDatabase.getBalancesDao()

}
