package com.paySera.supperApp.core.di.modules


import com.facebook.stetho.okhttp3.StethoInterceptor
import com.paySera.supperApp.core.BuildConfig
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit.SECONDS
import javax.inject.Singleton
import javax.net.ssl.*
import javax.net.ssl.SSLContext

import javax.net.ssl.X509TrustManager

import javax.net.ssl.TrustManager





@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideOkHttp(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .apply {
                addInterceptor(loggingInterceptor)
                addNetworkInterceptor(StethoInterceptor())
                connectionPool(ConnectionPool(100, 30L, SECONDS))
                readTimeout(30L, SECONDS)
                writeTimeout(30L, SECONDS)
                connectTimeout(30L, SECONDS)
                callTimeout(30L, SECONDS)
            }.build()
    }


    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .apply {
                baseUrl(BuildConfig.BASE_URL)
                client(okHttpClient)
                addConverterFactory(GsonConverterFactory.create())
            }.build()
    }

    @Singleton
    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE
        }
    }
}

