package com.paySera.supperApp.core

import android.app.Application
import android.content.Context
import com.paySera.supperApp.core.di.modules.*
import dagger.Component
import com.paySera.supperApp.common.di.qualifier.ApplicationContext
import com.paySera.supperApp.common.thread.DefaultDispatcher
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.thread.MainDispatcher
import com.paySera.supperApp.data.dao.BalancesDao
import com.paySera.supperApp.data.dao.CurrencyRateDao
import com.paySera.supperApp.data.pref.PreferenceStorage
import com.paySera.supperApp.data.di.ModuleRepositoryPaySera
import com.paySera.supperApp.domain.repository.BalancesRepository
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import com.paySera.supperApp.domain.repository.XChangesRepository
import kotlinx.coroutines.CoroutineDispatcher
import retrofit2.Retrofit
import javax.inject.Singleton

@Component(
    modules = [
        NetworkModule::class,
        DatabaseModule::class,
        ContextModule::class,
        CoroutinesModule::class,
        ModuleRepositoryPaySera::class,
        PreferenceStorageModule::class]
)
@Singleton
interface CoreComponent {
    @ApplicationContext
    fun provideContext(): Context

    fun application(): Application

    @IoDispatcher
    fun getIoDispatcher(): CoroutineDispatcher

    @MainDispatcher
    fun getMainDispatcher(): CoroutineDispatcher

    @DefaultDispatcher
    fun getDefaultDispatcher(): CoroutineDispatcher
    fun getPreferenceStorage(): PreferenceStorage
    fun getCurrencyRatesRepository(): CurrencyRatesRepository
    fun getBalancesRepository(): BalancesRepository
    fun getXChangesRepository(): XChangesRepository
    fun getRetrofit(): Retrofit
    fun getCurrencyRateDao(): CurrencyRateDao
    fun getBalancesDao(): BalancesDao
}
