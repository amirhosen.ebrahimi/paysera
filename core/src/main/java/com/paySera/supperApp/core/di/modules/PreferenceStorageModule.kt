package com.paySera.supperApp.core.di.modules

import android.content.Context
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.preferencesDataStore
import dagger.Module
import dagger.Provides
import com.paySera.supperApp.common.di.qualifier.ApplicationContext
import com.paySera.supperApp.data.prefs.SharedPreferenceStorage
import com.paySera.supperApp.data.pref.PreferenceStorage
import com.paySera.supperApp.core.BuildConfig
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@Module
class PreferenceStorageModule {

    val Context.dataStore by preferencesDataStore(
        name = BuildConfig.PREFS_NAME,
        produceMigrations = { context ->
            listOf(
                SharedPreferencesMigration(
                    context,
                    BuildConfig.PREFS_NAME
                )
            )
        }
    )
    @ExperimentalCoroutinesApi
    @FlowPreview
    @Provides
    @Singleton
    fun providePreferenceStorage(@ApplicationContext context: Context): PreferenceStorage {
        return SharedPreferenceStorage(context.dataStore)
    }

}