package com.paySera.supperApp.core.di.modules

import dagger.Module
import dagger.Provides
import com.paySera.supperApp.common.thread.DefaultDispatcher
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.thread.MainDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
object CoroutinesModule {

    @DefaultDispatcher
    @Provides
    @Singleton
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @IoDispatcher
    @Provides
    @Singleton
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @MainDispatcher
    @Provides
    @Singleton
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main
}
