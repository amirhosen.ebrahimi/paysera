import dependencies.Libs
import dependencies.Libs.DependencyInjection.Dagger
import dependencies.Tools


plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
        id(PARCELIZE)
        id(KOTLIN_KAPT)

    }
}

android {
    GradleVersionConfig.apply {
        packagingOptions.exclude(PackagingOptions.DEPENDENCIES)
        packagingOptions.exclude(PackagingOptions.LICENSE)
        packagingOptions.exclude(PackagingOptions.LICENSE_TEXT)
        packagingOptions.exclude(PackagingOptions.LICENSE_TEXT_2)
        packagingOptions.exclude(PackagingOptions.NOTICE)
        packagingOptions.exclude(PackagingOptions.NOTICE_TEXT)
        packagingOptions.exclude(PackagingOptions.NOTICE_TEXT_2)
        packagingOptions.exclude(PackagingOptions.ASL2)
        packagingOptions.exclude(PackagingOptions.DATA_DEBUG)
        packagingOptions.exclude(PackagingOptions.AL2)
        packagingOptions.exclude(PackagingOptions.KOTLIN)
        packagingOptions.exclude(PackagingOptions.LGPL2)

        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion = BUILD_TOOLS_VERSION
        testOptions.unitTests.isReturnDefaultValues = true

        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = 1
            versionName = "1.0"
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER

            buildConfigField("String", "BASE_URL", "\"http://api.exchangeratesapi.io/\"")
            buildConfigField("String", "DATABASE_NAME", "\"PaySera-db\"")
            buildConfigField("String", "PREFS_NAME", "\"PaySera-pref\"")
        }
        android.buildFeatures.dataBinding = true
        compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
        compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
        testOptions.animationsDisabled = true
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }
}

dependencies {

    Modules.run {
        implementation(project(DOMAIN))
        implementation(project(DATA))
    }
    Modules.Common.run {
        implementation(project(THREAD))
        implementation(project(DI))
    }

    Libs.AndroidX.Room.run {
        implementation(core)
        kapt(compiler)
    }

    implementation(Libs.AndroidX.paging)
    implementation(Libs.Network.OkHttp.core)
    implementation(Libs.Network.OkHttp.logger)
    implementation(Libs.Network.Retrofit.core)
    implementation(Libs.Network.Retrofit.gsonConverter)

    Libs.AndroidX.LifeCycle.run {
        implementation(commonJava8)
        implementation(liveData)
        implementation(runtime)
        implementation(viewModel)
    }
    implementation(Libs.AndroidX.extensionsCore)
    implementation(Libs.Kotlin.kotlin_stdlib)
    implementation(Libs.AndroidX.LifeCycle.lifecycle_extensions)
    implementation(Libs.Kotlin.Coroutine.core)
    implementation(Libs.Kotlin.Coroutine.android)
    implementation(Libs.Stetho.okHttp)
    implementation(Libs.Stetho.core)

    Dagger.run {
        implementation(android)
        kapt(compiler)
    }
    Libs.AndroidX.DataStore.run {
        implementation(core)
        implementation(preferences)
    }

}