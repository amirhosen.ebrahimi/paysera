buildscript {
    repositories {
        google()
        mavenCentral()
     }
    dependencies {
        classpath(GradlePlugins.ANDROID_GRADLE)
        classpath(GradlePlugins.KOTLIN_GRADLE)
        classpath(GradlePlugins.SAFE_ARGS)
        classpath(GradlePlugins.KOTLIN_GRADLE)
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.6.10")
    }
}
//allprojects {
//    configurations.all {
//        resolutionStrategy.force("org.objenesis:objenesis:2.6")
//    }
//    repositories {
//        google()
//        mavenCentral()
//        jcenter()
//        maven("../sdk724-repo")
//        maven("https://jitpack.io")
//        maven("https://kotlin.bintray.com/kotlinx")
//    }
//}
tasks.register("clean").configure {
    delete(rootProject.buildDir)
}