package com.paySera.supperApp.designSystem

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText

class PaySeraEditText : androidx.appcompat.widget.AppCompatEditText {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
}