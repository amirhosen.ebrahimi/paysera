package com.paySera.supperApp.designSystem

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.button.MaterialButton

class PaySeraButton : MaterialButton {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

}