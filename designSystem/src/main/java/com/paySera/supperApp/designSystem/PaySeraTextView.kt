package com.paySera.supperApp.designSystem

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textview.MaterialTextView

class PaySeraTextView  @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MaterialTextView(context, attrs, defStyleAttr)