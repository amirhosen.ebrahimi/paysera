package com.paySera.supperApp.data.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.domain.entity.Balances
import javax.inject.Inject

class BalancesEntityToBalances @Inject constructor() : Mapper<BalancesEntity, Balances> {
    override fun map(first: BalancesEntity): Balances {
        return Balances(
            first.name, first.amount
        )
    }
}