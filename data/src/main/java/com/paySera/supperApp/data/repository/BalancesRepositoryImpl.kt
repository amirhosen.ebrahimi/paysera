package com.paySera.supperApp.data.repository

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.dataSource.LocalBalancesDataSourceReadable
import com.paySera.supperApp.data.dataSource.LocalBalancesDataSourceWritable
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.repository.BalancesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import java.lang.Exception
import javax.inject.Inject

class BalancesRepositoryImpl @Inject constructor(
    private val localBalancesDataSourceWritable: LocalBalancesDataSourceWritable,
    private val localBalancesDataSourceReadable: LocalBalancesDataSourceReadable,
    private val balancesToBalancesEntity: Mapper<Balances, BalancesEntity>,
    private val balancesEntityToBalances: Mapper<BalancesEntity, Balances>,
) : BalancesRepository {
    override fun getAllBalances(): Flow<Resource<List<Balances>>> {
        return localBalancesDataSourceReadable.read(Unit)
            .flowOn(Dispatchers.IO)
            .map {
                Resource.success(it.map { balancesEntityToBalances.map(it) })
            }.catch {
                emit(Resource.error(Exception(it)))
            }
    }

    override suspend fun saveBalances(balances: Balances) {
        localBalancesDataSourceWritable.write(balancesToBalancesEntity.map(balances))
    }
}