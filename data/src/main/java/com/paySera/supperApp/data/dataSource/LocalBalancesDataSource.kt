package com.paySera.supperApp.data.dataSource

import com.paySera.supperApp.data.dao.BalancesDao
import com.paySera.supperApp.data.dao.CurrencyRateDao
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.datasource.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalBalancesDataSource @Inject constructor(
    private val balancesDao: BalancesDao
) : LocalBalancesDataSourceWritable, LocalBalancesDataSourceReadable {
    override fun read(input: Unit): Flow<List<BalancesEntity>> {
        return balancesDao.getAll()
    }

    override suspend fun write(input: BalancesEntity): Boolean {
        balancesDao.insert(input)
        return true
    }

}

interface LocalBalancesDataSourceWritable :
    Writable.Suspendable.IO<BalancesEntity, Boolean>

interface LocalBalancesDataSourceReadable :
    Readable.IO<Unit, @JvmSuppressWildcards Flow<List<BalancesEntity>>>