package com.paySera.supperApp.data.dataSource

import com.paySera.supperApp.data.pref.PreferenceStorage
import com.paySera.supperApp.datasource.Readable
import com.paySera.supperApp.datasource.Writable
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class LocalXChangeDataSource @Inject constructor(
    private val preferenceStorage: PreferenceStorage
) : LocalXChangeDataSourceWritable, LocalXChangeDataSourceReadable {
    override fun read(): Flow<Int> {
        return preferenceStorage.xChangeNumber
    }

    override suspend fun write(input: Int): Boolean {
        preferenceStorage.saveXChangeNumber(input)
        return true
    }


}

interface LocalXChangeDataSourceWritable :
    Writable.Suspendable.IO<Int, Boolean>

interface LocalXChangeDataSourceReadable :
    Readable< @JvmSuppressWildcards Flow<Int>>