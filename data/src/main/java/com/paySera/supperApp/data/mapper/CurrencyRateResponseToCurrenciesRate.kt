package com.paySera.supperApp.data.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import javax.inject.Inject

class CurrencyRateResponseToCurrenciesRate @Inject constructor() :
    Mapper<CurrencyRateResponse?, CurrenciesRate?> {
    override fun map(first: CurrencyRateResponse?): CurrenciesRate {
        val currencyList = mutableListOf<CurrencyRate>()
        first?.rates?.forEach { (s, d) ->
            currencyList.add(CurrencyRate(s, d, s == first.base))
        }

        return CurrenciesRate(
            base = first?.base,
            currencyList = currencyList
        )
    }
}