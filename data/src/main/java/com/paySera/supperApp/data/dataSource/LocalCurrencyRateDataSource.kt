package com.paySera.supperApp.data.dataSource

import com.paySera.supperApp.data.dao.CurrencyRateDao
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.datasource.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LocalCurrencyRateDataSource @Inject constructor(
    private val currencyRateDao: CurrencyRateDao
) : LocalCurrencyRateDataSourceWritable, LocalCurrencyRateDataSourceReadable {
    override suspend fun write(input: List<CurrencyRateEntity>): Boolean {
        currencyRateDao.insertAll(input)
        return true
    }

    override fun read(input: Unit): Flow<List<CurrencyRateEntity>> {
        return currencyRateDao.getAll()
    }
}

interface LocalCurrencyRateDataSourceWritable :
    Writable.Suspendable.IO<@JvmSuppressWildcards List<CurrencyRateEntity>, Boolean>

interface LocalCurrencyRateDataSourceReadable :
    Readable.IO<Unit, @JvmSuppressWildcards Flow<List<CurrencyRateEntity>>>