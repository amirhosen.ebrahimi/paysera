package com.paySera.supperApp.data.di

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.dataSource.*
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.data.mapper.*
import com.paySera.supperApp.data.pref.PreferenceStorage
import com.paySera.supperApp.data.prefs.SharedPreferenceStorage
import com.paySera.supperApp.data.repository.BalancesRepositoryImpl
import com.paySera.supperApp.data.repository.CurrencyRatesRepositoryImpl
import com.paySera.supperApp.data.repository.XChangesRepositoryImpl
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.data.webApi.service.CurrencyRateService
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.domain.repository.BalancesRepository
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import com.paySera.supperApp.domain.repository.XChangesRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
abstract class ModuleRepositoryPaySera {

    //......................Mappers...........................
    @Binds
    abstract fun bindCurrencyRateResponseToCurrenciesRate(mapper: CurrencyRateResponseToCurrenciesRate): Mapper<CurrencyRateResponse?, CurrenciesRate?>

    @Binds
    abstract fun bindCurrencyRateEntityToCurrencyRate(mapper: CurrencyRateEntityToCurrencyRate): Mapper<CurrencyRateEntity, CurrencyRate>

    @Binds
    abstract fun bindCurrenciesRateToCurrencyRateLocal(mapper: CurrenciesRateToCurrencyRateLocal): Mapper<CurrenciesRate?, List<CurrencyRateEntity>>

    @Binds
    abstract fun bindBalancesToBalancesEntity(mapper: BalancesToBalancesEntity): Mapper<Balances, BalancesEntity>

    @Binds
    abstract fun bindBalancesEntityToBalances(mapper: BalancesEntityToBalances): Mapper<BalancesEntity, Balances>
    //......................DataSources...........................

    @Binds
    abstract fun bindLocalXChangeDataSourceReadable(dataSource: LocalXChangeDataSource): LocalXChangeDataSourceReadable

    @Binds
    abstract fun bindLocalXChangeDataSourceWritable(dataSource: LocalXChangeDataSource): LocalXChangeDataSourceWritable

    @Binds
    abstract fun bindRemoteCurrencyRateDataSourceReadable(dataSource: RemoteCurrencyRateSource): RemoteCurrencyRateDataSourceReadable

    @Binds
    abstract fun bindLocalBalancesDataSourceWritable(dataSource: LocalBalancesDataSource): LocalBalancesDataSourceWritable

    @Binds
    abstract fun bindLocalBalancesDataSourceReadable(dataSource: LocalBalancesDataSource): LocalBalancesDataSourceReadable

    @Binds
    abstract fun bindLocalCurrencyRateDataSourceWritable(dataSource: LocalCurrencyRateDataSource): LocalCurrencyRateDataSourceWritable

    @Binds
    abstract fun bindLocalCurrencyRateDataSourceReadable(dataSource: LocalCurrencyRateDataSource): LocalCurrencyRateDataSourceReadable

    //......................Repositories...........................
    @Binds
    abstract fun bindCurrencyRatesRepository(repo: CurrencyRatesRepositoryImpl): CurrencyRatesRepository

    @Binds
    abstract fun bindBalancesRepository(repo: BalancesRepositoryImpl): BalancesRepository

    @Binds
    abstract fun bindXChangesRepositoryImpl(repo: XChangesRepositoryImpl): XChangesRepository

    companion object {
        @Provides
        fun provideApiService(retrofit: Retrofit): CurrencyRateService {
            return retrofit.create(CurrencyRateService::class.java)
        }
    }

}