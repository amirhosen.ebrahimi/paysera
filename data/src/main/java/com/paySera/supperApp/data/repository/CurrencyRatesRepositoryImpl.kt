package com.paySera.supperApp.data.repository

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.dataSource.LocalCurrencyRateDataSourceReadable
import com.paySera.supperApp.data.dataSource.LocalCurrencyRateDataSourceWritable
import com.paySera.supperApp.data.dataSource.RemoteCurrencyRateDataSourceReadable
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import java.lang.Exception
import javax.inject.Inject

class CurrencyRatesRepositoryImpl @Inject constructor(
    private val remoteCurrencyRateDataSourceReadable: RemoteCurrencyRateDataSourceReadable,
    private val localCurrencyRateDataSourceWritable: LocalCurrencyRateDataSourceWritable,
    private val localCurrencyRateDataSourceReadable: LocalCurrencyRateDataSourceReadable,
    private val currencyRateResponseToCurrenciesRate: Mapper<CurrencyRateResponse?, CurrenciesRate?>,
    private val currenciesRateToCurrencyRateLocal: Mapper<CurrenciesRate?, List<CurrencyRateEntity>>,
    private val currencyRateEntityToCurrencyRate: Mapper<CurrencyRateEntity, CurrencyRate>,
) : CurrencyRatesRepository {

    override fun getLatestCurrencyRates(): Flow<Resource<CurrenciesRate?>> {
        return flow {
            emit(remoteCurrencyRateDataSourceReadable.read(Unit).body())
        }.flowOn(Dispatchers.IO).map {
            Resource.success(currencyRateResponseToCurrenciesRate.map(it))
        }.catch {
            emit(Resource.error(Exception(it)))
        }
    }

    override fun getCurrencyRates(): Flow<Resource<List<CurrencyRate>>> {
        return localCurrencyRateDataSourceReadable.read(Unit)
            .flowOn(Dispatchers.IO)
            .map {
                Resource.success(it.map { currencyRateEntityToCurrencyRate.map(it) })
            }.catch {
                emit(Resource.error(Exception(it)))
            }
    }

    override suspend fun saveLatestCurrencyRates(currenciesRate: CurrenciesRate): Boolean {
        return localCurrencyRateDataSourceWritable.write(
            currenciesRateToCurrencyRateLocal.map(
                currenciesRate
            )
        )
    }

}