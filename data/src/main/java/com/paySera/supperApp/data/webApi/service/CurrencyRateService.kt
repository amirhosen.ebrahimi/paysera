package com.paySera.supperApp.data.webApi.service

import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import retrofit2.http.GET
import retrofit2.Response

interface CurrencyRateService {
    @GET("v1/latest?access_key=6f7225a86a2c988095653d2deaa625c5&format=1")
    suspend fun getLatestCurrencyRate(): Response<CurrencyRateResponse>
}

