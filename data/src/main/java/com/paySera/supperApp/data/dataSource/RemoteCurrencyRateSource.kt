package com.paySera.supperApp.data.dataSource

import retrofit2.Response
import javax.inject.Inject
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.data.webApi.service.CurrencyRateService
import com.paySera.supperApp.datasource.*

class RemoteCurrencyRateSource @Inject constructor(
    private val currencyRateService: CurrencyRateService
) : RemoteCurrencyRateDataSourceReadable {

    override suspend fun read(input: Unit): Response<CurrencyRateResponse> {
        return currencyRateService.getLatestCurrencyRate()
    }

}

interface RemoteCurrencyRateDataSourceReadable :
    Readable.Suspendable.IO<Unit, Response<CurrencyRateResponse>>

