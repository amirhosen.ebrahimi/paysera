package com.paySera.supperApp.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CurrencyRateDao : BaseDao<CurrencyRateEntity>{
    @Query("SELECT * FROM CurrencyRateEntity")
    fun getAll(): Flow<List<CurrencyRateEntity>>
}