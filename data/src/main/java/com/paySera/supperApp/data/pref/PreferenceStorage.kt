package com.paySera.supperApp.data.pref

import kotlinx.coroutines.flow.Flow

interface PreferenceStorage {
    suspend fun saveXChangeNumber(prefer: Int)
    val xChangeNumber: Flow<Int>
}