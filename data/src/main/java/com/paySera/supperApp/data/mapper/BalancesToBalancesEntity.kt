package com.paySera.supperApp.data.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.domain.entity.Balances
import javax.inject.Inject

class BalancesToBalancesEntity @Inject constructor() : Mapper<Balances, BalancesEntity> {
    override fun map(first: Balances): BalancesEntity {
        return BalancesEntity(
            first.name, first.amount
        )
    }
}