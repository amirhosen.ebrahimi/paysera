package com.paySera.supperApp.data.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import javax.inject.Inject

class CurrencyRateEntityToCurrencyRate @Inject constructor() :
    Mapper<CurrencyRateEntity,CurrencyRate> {
    override fun map(first: CurrencyRateEntity): CurrencyRate {
        return CurrencyRate(
            first.name,
            first.rate,
            first.isBase
        )
    }


}