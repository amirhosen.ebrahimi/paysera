package com.paySera.supperApp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.paySera.supperApp.data.dao.BalancesDao
import com.paySera.supperApp.data.dao.CurrencyRateDao
import com.paySera.supperApp.data.etity.BalancesEntity
import com.paySera.supperApp.data.etity.CurrencyRateEntity

@Database(
    entities = [
        CurrencyRateEntity::class, BalancesEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class PaySeraDataBase : RoomDatabase() {
    abstract fun getCurrencyRateDao(): CurrencyRateDao
    abstract fun getBalancesDao(): BalancesDao
}