package com.paySera.supperApp.data.etity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CurrencyRateEntity(
    @PrimaryKey
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "rate")
    val rate: Double,
    @ColumnInfo(name = "is_base")
    val isBase: Boolean
)
