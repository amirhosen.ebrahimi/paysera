package com.paySera.supperApp.data.prefs

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.paySera.supperApp.data.pref.PreferenceStorage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@FlowPreview
@ExperimentalCoroutinesApi
class SharedPreferenceStorage @Inject constructor(
    private val dataStore: DataStore<androidx.datastore.preferences.core.Preferences>
) : PreferenceStorage {

    override suspend fun saveXChangeNumber(prefer: Int) {
        dataStore.edit {
            it[PreferencesKeys.XCHANGE_NUMBER_KEY] = prefer
        }
    }

    override var xChangeNumber: Flow<Int> = dataStore.data.map {
        it[PreferencesKeys.XCHANGE_NUMBER_KEY] ?: 0
    }

    object PreferencesKeys {
        val XCHANGE_NUMBER_KEY = intPreferencesKey("xChange-number-key")
    }


}
