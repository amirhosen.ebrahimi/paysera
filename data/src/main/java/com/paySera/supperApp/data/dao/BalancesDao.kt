package com.paySera.supperApp.data.dao

import androidx.room.Dao
import androidx.room.Query
import com.paySera.supperApp.data.etity.BalancesEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface BalancesDao : BaseDao<BalancesEntity>{
    @Query("SELECT * FROM BalancesEntity")
    fun getAll(): Flow<List<BalancesEntity>>
}