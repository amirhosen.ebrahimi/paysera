package com.paySera.supperApp.data.repository

import com.paySera.supperApp.data.dataSource.LocalXChangeDataSourceReadable
import com.paySera.supperApp.data.dataSource.LocalXChangeDataSourceWritable
import com.paySera.supperApp.domain.repository.XChangesRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class XChangesRepositoryImpl @Inject constructor(
    private val localXChangeDataSourceWritable: LocalXChangeDataSourceWritable,
    private val localXChangeDataSourceReadable: LocalXChangeDataSourceReadable
) : XChangesRepository {

    override fun getNumberOfXChanges(): Flow<Int> {
        return localXChangeDataSourceReadable.read()
    }

    override suspend fun saveNumberOfXChanges(input: Int): Boolean {
        return localXChangeDataSourceWritable.write(input)
    }
}