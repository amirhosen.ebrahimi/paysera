package com.paySera.supperApp.data.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.data.etity.CurrencyRateEntity
import com.paySera.supperApp.data.webApi.response.CurrencyRateResponse
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import javax.inject.Inject

class CurrenciesRateToCurrencyRateLocal @Inject constructor() :
    Mapper<CurrenciesRate?,@JvmSuppressWildcards List<CurrencyRateEntity>> {

    override fun map(first: CurrenciesRate?): List<CurrencyRateEntity> {
        return first?.currencyList?.map {
            CurrencyRateEntity(it.name, it.rate, it.name == first.base)
        } ?: emptyList()
    }
}