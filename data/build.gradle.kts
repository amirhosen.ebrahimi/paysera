import dependencies.Libs

plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
        id(PARCELIZE)
        id(KOTLIN_KAPT)
        id(NAVIGATION_SAFEARGS_KOTLIN)
    }
}
android {
    GradleVersionConfig.apply {
        buildToolsVersion = BUILD_TOOLS_VERSION
        compileSdkVersion(COMPILE_SDK_VERSION)
        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = 1
            versionName = "1.0"
        }
    }
    buildFeatures.viewBinding = true
    compileOptions.sourceCompatibility = JavaVersion.VERSION_1_8
    compileOptions.targetCompatibility = JavaVersion.VERSION_1_8
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}
dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    api(project(Modules.Common.DATASOURCE))
    implementation(project(Modules.Common.USECAES))
    api(project(Modules.Common.MAPPER))
    implementation(project(Modules.DOMAIN))
    //Kotlin
    implementation(Libs.Kotlin.kotlin_stdlib)
    //Network
    Libs.Network.OkHttp.run {
        implementation(core)
        implementation(logger)
    }
    Libs.Network.Retrofit.run {
        implementation(core)
        implementation(gsonConverter)
    }
    //Dagger
    Libs.DependencyInjection.Dagger.run {
        implementation(runtime)
        implementation(android)
        implementation(android_support)
        kapt(compiler)
        kapt(android_support_compiler)
    }
    Libs.AndroidX.Room.run {
        implementation(core)
        implementation(runtime)
        kapt(compiler)
    }
    Libs.AndroidX.DataStore.run {
        implementation(core)
        implementation(preferences)
    }
}
