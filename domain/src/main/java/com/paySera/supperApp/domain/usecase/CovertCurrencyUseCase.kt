package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.SuspendUseCase
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CovertCurrency
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.domain.repository.BalancesRepository
import com.paySera.supperApp.domain.repository.XChangesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class CovertCurrencyUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val calculateNewBalanceUseCase: CalculateNewBalanceUseCase,
    private val xChangesRepositoryImpl: XChangesRepository,
    private val getAllBalancesUseCase: GetAllBalancesUseCase,
    private val balancesRepository: BalancesRepository,
) : SuspendUseCase<CovertCurrencyUseCase.Params, CovertCurrency>(
    dispatcher
) {

    override suspend fun execute(parameters: Params): CovertCurrency {
        // get user data
        val xChangeNumber = xChangesRepositoryImpl.getNumberOfXChanges().first()
        val allBalances = getAllBalancesUseCase(Unit).first()

        val userSellCurrency =
            allBalances.data?.firstOrNull { it.name == parameters.sellCurrency.name }
                ?: throw Exception(
                    ("you not have " + parameters.sellCurrency.name)
                )

        if (userSellCurrency.amount < parameters.amount)
            throw Exception(
                ("Your " + parameters.sellCurrency.name + " credit is not enough")
            )


        val amountOfXChange = if (xChangeNumber > 5)
            0.7
        else
            0.0
        val userEUR = allBalances.data?.firstOrNull { it.name == commissionFreeCurrency }
        var userEurAmount = userEUR?.amount
        if (parameters.sellCurrency.name == commissionFreeCurrency)
            if (userEurAmount != null) {
                userEurAmount -= (parameters.amount)
            }

        if (userEurAmount ?: 0.0 < amountOfXChange) {
            throw Exception(
                ("Your " + userEUR?.name + " credit is not enough for the currency exchange operation")
            )
        }

        // update receive balance
        val receiveAmount = calculateNewBalanceUseCase(
            CalculateNewBalanceUseCase.Params(
                parameters.sellCurrency.rate,
                parameters.receiveCurrency.rate,
                parameters.amount
            )
        )
        val currentUserReceiveBalance =
            allBalances.data?.firstOrNull { it.name == parameters.receiveCurrency.name }
        val newCurrentUserReceiveBalance =
            (currentUserReceiveBalance?.amount ?: 0.0).plus(receiveAmount.data ?: 0.0)

        balancesRepository.saveBalances(
            Balances(
                parameters.receiveCurrency.name,
                newCurrentUserReceiveBalance
            )
        )

        // update sell balance
        val newCurrentUserSellBalance =
            userSellCurrency.amount.minus(parameters.amount)
        balancesRepository.saveBalances(
            Balances(
                parameters.sellCurrency.name,
                newCurrentUserSellBalance
            )
        )

        // update EUR for currency exchange operation
        val newUserEUR =
            getAllBalancesUseCase(Unit).first().data?.firstOrNull { it.name == commissionFreeCurrency }
        balancesRepository.saveBalances(
            Balances(
                newUserEUR?.name ?: commissionFreeCurrency,
                newUserEUR?.amount?.minus(amountOfXChange) ?: 0.0
            )
        )
        xChangesRepositoryImpl.saveNumberOfXChanges(xChangeNumber + 1)
        return CovertCurrency(
            Balances(
                parameters.sellCurrency.name, newCurrentUserSellBalance
            ),
            Balances(parameters.receiveCurrency.name, newCurrentUserReceiveBalance),
            amountOfXChange,
            commissionFreeCurrency
        )

    }

    data class Params(
        val sellCurrency: CurrencyRate,
        val receiveCurrency: CurrencyRate,
        val amount: Double
    )

    companion object {
        val commissionFreeCurrency = "EUR"
    }
}