package com.paySera.supperApp.domain.repository

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import kotlinx.coroutines.flow.Flow

interface CurrencyRatesRepository {
    fun getLatestCurrencyRates(): Flow<Resource<CurrenciesRate?>>
    fun getCurrencyRates(): Flow<Resource<List<CurrencyRate>>>
    suspend fun saveLatestCurrencyRates(currenciesRate: CurrenciesRate): Boolean
}