package com.paySera.supperApp.domain.entity

data class Balances(val name: String, val amount: Double)