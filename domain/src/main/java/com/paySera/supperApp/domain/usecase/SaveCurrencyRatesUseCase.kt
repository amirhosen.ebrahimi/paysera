package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.FlowUseCase
import com.paySera.supperApp.common.usecase.SuspendUseCase
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SaveCurrencyRatesUseCase @Inject constructor(
    private val locationRepository: CurrencyRatesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : SuspendUseCase<CurrenciesRate, Boolean>(dispatcher) {
    override suspend fun execute(parameters: CurrenciesRate): Boolean {
       return locationRepository.saveLatestCurrencyRates(parameters)
    }


}