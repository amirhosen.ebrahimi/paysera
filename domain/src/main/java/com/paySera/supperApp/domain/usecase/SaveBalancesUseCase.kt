package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.FlowUseCase
import com.paySera.supperApp.common.usecase.SuspendUseCase
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.repository.BalancesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SaveBalancesUseCase @Inject constructor(
    private val balancesRepository: BalancesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : SuspendUseCase<Balances, Unit>(dispatcher) {
    override suspend fun execute(parameters: Balances) {
        balancesRepository.saveBalances(parameters)
    }


}