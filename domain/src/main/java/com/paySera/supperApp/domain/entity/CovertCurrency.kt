package com.paySera.supperApp.domain.entity

data class CovertCurrency(
    val sellBalances: Balances,
    val receiveBalances: Balances,
    val commissionFreeCurrencyAmount: Double,
    val commissionFreeCurrencyName: String,
)