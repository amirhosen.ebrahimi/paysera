package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.FlowUseCase
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLatestCurrencyRatesUseCase @Inject constructor(
    private val locationRepository: CurrencyRatesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<Unit, CurrenciesRate?>(dispatcher) {

    override fun execute(parameters: Unit): Flow<Resource<CurrenciesRate?>> {
        return locationRepository.getLatestCurrencyRates()
    }
}