package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.SuspendUseCase
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class CalculateNewBalanceUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
) : SuspendUseCase<CalculateNewBalanceUseCase.Params, Double>(
    dispatcher
) {
    override suspend fun execute(parameters: Params): Double {
        return ((parameters.amount * parameters.sellRate) / parameters.receiveRate)
    }

    data class Params(val sellRate: Double, val receiveRate: Double, val amount: Double)

}