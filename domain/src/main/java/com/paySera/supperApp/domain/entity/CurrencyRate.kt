package com.paySera.supperApp.domain.entity

data class CurrencyRate(
    val name: String,
    val rate: Double,
    val isBase : Boolean
)
