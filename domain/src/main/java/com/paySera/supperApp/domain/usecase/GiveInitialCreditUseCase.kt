package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.onSuccess
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.FlowUseCase
import com.paySera.supperApp.common.usecase.SuspendUseCase
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.repository.BalancesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

class GiveInitialCreditUseCase@Inject constructor(
    private val balancesRepository: BalancesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : SuspendUseCase<Unit, Unit>(dispatcher) {
    override suspend fun execute(parameters: Unit) {
        balancesRepository.getAllBalances().collectLatest {
            it.onSuccess {
                if (it.isEmpty())
                    balancesRepository.saveBalances(Balances("EUR",1000.0))
            }
        }
    }


}