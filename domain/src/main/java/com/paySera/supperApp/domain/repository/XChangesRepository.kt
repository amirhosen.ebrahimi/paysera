package com.paySera.supperApp.domain.repository

import kotlinx.coroutines.flow.Flow

interface XChangesRepository {
    fun getNumberOfXChanges(): Flow<Int>
    suspend fun saveNumberOfXChanges(input: Int): Boolean
}