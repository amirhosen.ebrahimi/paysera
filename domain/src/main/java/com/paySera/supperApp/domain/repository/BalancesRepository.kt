package com.paySera.supperApp.domain.repository

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.domain.entity.Balances
import kotlinx.coroutines.flow.Flow

interface BalancesRepository {
    fun getAllBalances(): Flow<Resource<List<Balances>>>
    suspend fun saveBalances(balances: Balances)
}