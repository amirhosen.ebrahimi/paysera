package com.paySera.supperApp.domain.entity

data class CurrenciesRate(
    val currencyList: List<CurrencyRate>,
    val base: String? = null
)