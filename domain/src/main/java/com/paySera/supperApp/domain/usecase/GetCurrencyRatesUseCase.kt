package com.paySera.supperApp.domain.usecase

import com.paySera.supperApp.common.Resource
import com.paySera.supperApp.common.thread.IoDispatcher
import com.paySera.supperApp.common.usecase.FlowUseCase
import com.paySera.supperApp.domain.entity.CurrenciesRate
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.domain.repository.CurrencyRatesRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCurrencyRatesUseCase @Inject constructor(
    private val locationRepository: CurrencyRatesRepository,
    @IoDispatcher dispatcher: CoroutineDispatcher
) : FlowUseCase<Unit, List<CurrencyRate>>(dispatcher) {

    override fun execute(parameters: Unit): Flow<Resource<List<CurrencyRate>>> {
        return locationRepository.getCurrencyRates()
    }
}