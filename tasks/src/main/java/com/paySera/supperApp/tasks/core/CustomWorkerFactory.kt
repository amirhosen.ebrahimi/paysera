package com.paySera.supperApp.tasks.core

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.paySera.supperApp.tasks.updateCurrencyRates.presentation.UpdateCurrencyRatesWorker
import javax.inject.Inject

// todo amir
class WorkerFactory @Inject constructor(
    private val helloWorldWorkerFactory: UpdateCurrencyRatesWorker.Factory,
) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters,
    ): ListenableWorker? {
        return when (workerClassName) {
            UpdateCurrencyRatesWorker::class.java.name ->
                helloWorldWorkerFactory.create(appContext, workerParameters)
            else -> null
        }
    }
}