package com.paySera.supperApp.tasks.updateCurrencyRates.presentation.di

import com.paySera.supperApp.common.di.InjectingSavedStateViewModelFactory
import com.paySera.supperApp.common.di.ViewModelFactory
import dagger.Binds
import dagger.Module
import com.paySera.supperApp.common.mapper.Mapper

@Module
abstract class UpdateCurrencyRatesPresentationModule {

}