package com.paySera.supperApp.tasks.updateCurrencyRates.presentation

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.paySera.supperApp.common.onSuccess
import com.paySera.supperApp.domain.usecase.GetLatestCurrencyRatesUseCase
import com.paySera.supperApp.domain.usecase.SaveCurrencyRatesUseCase
//import com.paySera.supperApp.tasks.updateCurrencyRates.domail.usecase.GetLatestCurrencyRatesUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first

class UpdateCurrencyRatesWorker @AssistedInject constructor(
    @Assisted private val context: Context,
    @Assisted private val params: WorkerParameters,
    private val getLatestCurrencyRatesUseCase: GetLatestCurrencyRatesUseCase,
    private val saveCurrencyRatesUseCase: SaveCurrencyRatesUseCase
) : CoroutineWorker(context, params) {


    /**
     * class annotate with @AssistedFactory will available in the dependency graph, you don't need
     * additional binding from [UpdateCurrencyRatesWorker] to [Factory].
     */
    @AssistedFactory
    interface Factory {
        fun create(appContext: Context, params: WorkerParameters): UpdateCurrencyRatesWorker
    }

    override suspend fun doWork(): Result {
        while (true) {
            delay(5000)
            getLatestCurrencyRatesUseCase(Unit).first().onSuccess {
                it?.let {
                    saveCurrencyRatesUseCase(it)
                }
            }
        }
        return Result.success()
    }

}


