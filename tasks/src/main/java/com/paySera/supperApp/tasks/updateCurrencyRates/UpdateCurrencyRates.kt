package com.paySera.supperApp.tasks.updateCurrencyRates

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class UpdateCurrencyRates