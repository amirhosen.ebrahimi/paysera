import Modules.DESIGN_SYSTEM
import dependencies.Libs
import dependencies.Tools
import dependencies.Libs.DependencyInjection.Dagger

plugins {
    GradlePluginId.apply {
        id(ANDROID_LIBRARY)
        id(KOTLIN_ANDROID)
         id(PARCELIZE)
        id(KOTLIN_KAPT)
    }
}
android {
    GradleVersionConfig.apply {
        compileSdkVersion(COMPILE_SDK_VERSION)
        buildToolsVersion = BUILD_TOOLS_VERSION
        defaultConfig {
            minSdkVersion(MIN_SDK_VERSION)
            targetSdkVersion(TARGET_SDK_VERSION)
            versionCode = 1
            versionName = "1.0"
            testInstrumentationRunner = TEST_INSTRUMENTATION_RUNNER

        }
    }
}

dependencies {
    implementation(Libs.timber)

    Libs.AndroidX.WorkManager.run {
        implementation(workManagerKtx)
    }
    implementation(project(Modules.DOMAIN))

    Modules.Common.run {
        implementation(project(USECAES))
        implementation(project(MAPPER))
        implementation(project(THREAD))
        implementation(project(NETWORKING))
        implementation(project(DI))
    }
    //Dagger
    Dagger.run {
        implementation(runtime)
        implementation(android)
        implementation(android_support)
        kapt(compiler)
        kapt(android_support_compiler)
    }
    Dagger.Assisted.run {
        compileOnly(annotations)
        kapt(processor)
    }

}