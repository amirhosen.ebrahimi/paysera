package com.paySera.supperApp.common.mapper

interface Mapper<First, Second> {

    fun map(first: First): Second
}
