package com.paySera.supperApp.common.testshare

//
//@OptIn(ExperimentalCoroutinesApi::class)
//class MainCoroutineRule @ExperimentalCoroutinesApi constructor(
//    val testDispatcher: TestDispatcher = StandardTestDispatcher()
//) : TestWatcher() {
//
//    override fun starting(description: Description) {
//        super.starting(description)
//        Dispatchers.setMain(testDispatcher)
//    }
//
//    override fun finished(description: Description) {
//        super.finished(description)
//        Dispatchers.resetMain()
////        testDispatcher[]()
//    }
//}
//

//fun MainCoroutineRule.runBlockingTest(block: suspend () -> Unit) =
//    this.testDispatcher.runBlockingTest {
//        block()
//    }
//
///**
// * Creates a new [CoroutineScope] with the rule's testDispatcher
// */
//fun MainCoroutineRule.coroutineScope(): CoroutineScope = CoroutineScope(testDispatcher)
