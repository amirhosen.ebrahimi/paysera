package com.paySera.supperApp.common.testshare

fun assertThrows(v: Throwable?, e: Throwable?) {
    assert(((v?.javaClass == e?.javaClass) && (v?.message == e?.message)))
}