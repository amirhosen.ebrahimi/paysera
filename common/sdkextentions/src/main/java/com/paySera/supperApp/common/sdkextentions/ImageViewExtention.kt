package com.paySera.supperApp.common.sdkextentions

import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.paySera.supperApp.designSystem.PaySeraImageView

fun PaySeraImageView.loadImage(url: String?, placeholderId: Drawable? = null) {
    Glide.with(this)
        .load(url)
        .apply {
            placeholderId?.let { placeholder(it) }
        }
        .into(this)
}
