package com.paySera.supperApp.common

import com.paySera.supperApp.common.mapper.Mapper


data class Resource<out T>(val status: Status, val data: T?, val error: Exception?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(error: Exception?, data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                error
            )
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null
            )
        }
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}

fun <R> Resource<R>.isLoading(): Boolean {
    return this.status == Resource.Status.LOADING
}

fun <R> Resource<R>.isError(): Boolean {
    return this.status == Resource.Status.ERROR
}

fun <R> Resource<R>.isSuccess(): Boolean {
    return this.status == Resource.Status.SUCCESS
}

inline fun <R> Resource<R>.onLoading(action: (Boolean) -> Unit): Resource<R> {
    if (this.status == Resource.Status.LOADING) {
        action(true)
    } else action(false)
    return this
}

inline fun <R> Resource<R>.onError(action: (error: Exception, data: R?) -> Unit): Resource<R> {
    if (this.status == Resource.Status.ERROR) {
        error?.let { action(error, data) }
    }
    return this
}

inline fun <R> Resource<R>.onSuccess(action: (R) -> Unit): Resource<R> {
    if (this.status == Resource.Status.SUCCESS) {
        data?.let { action(it) }
    }
    return this
}

fun <R, T> Resource<R>.map(mapper: Mapper<R, T>): Resource<T> {
    return when {
        this.isSuccess() -> {
            if (data != null)
                Resource.success(mapper.map(data))
            else
                Resource.success(null)
        }
        this.isLoading() -> {
            Resource.loading((null))
        }
        else -> {
            Resource.error(error)
        }
    }
}