package com.paySera.supperApp.common.usecase

import com.paySera.supperApp.common.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import timber.log.Timber


abstract class SuspendUseCase<in P, R>(private val coroutineDispatcher: CoroutineDispatcher) {

    suspend operator fun invoke(parameters: P): Resource<R> {
        return try {
            withContext(coroutineDispatcher) {
                execute(parameters).let {
                    Resource.success(it)
                }
            }
        } catch (e: Exception) {
            Timber.d(e)
            Resource.error(e)
        }
    }


    @Throws(RuntimeException::class)
    protected abstract suspend fun execute(parameters: P): R
}
