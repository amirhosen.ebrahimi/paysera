package com.paySera.supperApp.placeList.presentation.entity

data class BalanceView(
    val name: String,
    val amount: Double
)