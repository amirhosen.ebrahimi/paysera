package com.paySera.supperApp.placeList.presentation.di

import com.paySera.supperApp.common.di.InjectingSavedStateViewModelFactory
import com.paySera.supperApp.common.di.ViewModelFactory
import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.placeList.PlaceListScope
import com.paySera.supperApp.placeList.presentation.entity.BalanceView
import com.paySera.supperApp.placeList.presentation.entity.CurrencyRateView
import com.paySera.supperApp.placeList.presentation.mapper.BalanceListToBalanceViewList
import com.paySera.supperApp.placeList.presentation.mapper.CurrencyRateToCurrencyRateView
import com.paySera.supperApp.placeList.presentation.mapper.CurrencyRateViewToCurrencyRate
import dagger.Binds
import dagger.Module

@Module
abstract class PlaceListPresentationModule {

    @Binds
    @PlaceListScope
    abstract fun bindCurrencyRateToCurrencyRateView(mapper: CurrencyRateToCurrencyRateView): Mapper<@JvmSuppressWildcards List<CurrencyRate>, @JvmSuppressWildcards List<CurrencyRateView>>

    @Binds
    @PlaceListScope
    abstract fun bindBalanceListToBalanceViewList(mapper: BalanceListToBalanceViewList): Mapper<@JvmSuppressWildcards List<Balances>, @JvmSuppressWildcards List<BalanceView>>

    @Binds
    @PlaceListScope
    abstract fun bindCurrencyRateViewToCurrencyRate(mapper: CurrencyRateViewToCurrencyRate): Mapper<CurrencyRateView, CurrencyRate>

    @Binds
    @PlaceListScope
    abstract fun bindViewModelFactory(factory: InjectingSavedStateViewModelFactory): ViewModelFactory

}