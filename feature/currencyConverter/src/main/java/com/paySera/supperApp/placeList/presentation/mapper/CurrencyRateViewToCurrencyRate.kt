package com.paySera.supperApp.placeList.presentation.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.placeList.presentation.entity.BalanceView
import com.paySera.supperApp.placeList.presentation.entity.CurrencyRateView
import javax.inject.Inject


class CurrencyRateViewToCurrencyRate @Inject constructor() :
    Mapper<CurrencyRateView, CurrencyRate> {
    override fun map(first: CurrencyRateView): CurrencyRate {
        return CurrencyRate(first.name, first.rate, first.isBase)
    }

}
