package com.paySera.supperApp.placeList.presentation.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.placeList.presentation.entity.BalanceView
import com.paySera.supperApp.placeList.presentation.entity.CurrencyRateView
import javax.inject.Inject

class CurrencyRateToCurrencyRateView @Inject constructor() :
    Mapper<@JvmSuppressWildcards List<CurrencyRate>,@JvmSuppressWildcards List<CurrencyRateView>> {
    override fun map(first: List<CurrencyRate>): List<CurrencyRateView> {
        return first.map {
            CurrencyRateView(it.name, it.rate, it.isBase)
        }
    }
}