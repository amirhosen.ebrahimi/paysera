package com.paySera.supperApp.placeList.presentation

import androidx.lifecycle.*
import com.paySera.supperApp.common.Resource
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import com.paySera.supperApp.common.di.AssistedSavedStateViewModelFactory
import com.paySera.supperApp.common.map
import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.domain.entity.CovertCurrency
import com.paySera.supperApp.domain.entity.CurrencyRate
import com.paySera.supperApp.domain.usecase.*
import com.paySera.supperApp.placeList.presentation.entity.BalanceView
import com.paySera.supperApp.placeList.presentation.entity.CurrencyRateView
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class CurrencyConverterViewModel @AssistedInject constructor(
    @Assisted private val savedStateHandle: SavedStateHandle,
    private val getCurrencyRatesUseCase: GetCurrencyRatesUseCase,
    private val getAllBalancesUseCase: GetAllBalancesUseCase,
    private val giveInitialCreditUseCase: GiveInitialCreditUseCase,
    private val calculateNewBalanceUseCase: CalculateNewBalanceUseCase,
    private val covertCurrencyUseCase: CovertCurrencyUseCase,
    private val balanceToBalanceView: Mapper<List<Balances>, List<BalanceView>>,
    private val currencyRateToCurrencyRateView: Mapper<List<CurrencyRate>, List<CurrencyRateView>>,
    private val currencyRateViewToCurrencyRate: Mapper<CurrencyRateView, CurrencyRate>,
) : ViewModel() {

    private val _currencyRates =
        MutableStateFlow<Resource<List<CurrencyRateView>>?>(Resource.success(null))
    val currencyRates: StateFlow<Resource<List<CurrencyRateView>>?> get() = _currencyRates

    private val _covertCurrency =
        MutableStateFlow<Resource<CovertCurrency>?>(Resource.success(null))
    val covertCurrency: StateFlow<Resource<CovertCurrency>?> get() = _covertCurrency

    private val _allBalances =
        MutableStateFlow<Resource<List<BalanceView>>>(Resource.success(null))
    val allBalances: StateFlow<Resource<List<BalanceView>>> get() = _allBalances

    private val _receiveMoney =
        MutableStateFlow<Resource<Double>>(Resource.success(null))
    val receiveMoney: StateFlow<Resource<Double>> get() = _receiveMoney

    init {
        viewModelScope.launch { giveInitialCreditUseCase(Unit) }
    }

    fun getCurrencyRates() {
        viewModelScope.launch {
            getCurrencyRatesUseCase(Unit).collectLatest {
                _currencyRates.emit(it.map(currencyRateToCurrencyRateView))
            }
        }
    }

    fun getAllBalances() {
        viewModelScope.launch {
            getAllBalancesUseCase(Unit).collectLatest {
                _allBalances.emit(it.map(balanceToBalanceView))
            }
        }
    }

    fun getCurrencySelected(selectedItemPosition: Int): CurrencyRateView? {
        if (selectedItemPosition >= 0)
            return _currencyRates.value?.data?.get(selectedItemPosition)
        return null
    }

    fun calculateNewBalance(
        text: String?,
        sellCurrencySelected: CurrencyRateView?,
        receiveCurrencySelected: CurrencyRateView?
    ) {
        if (sellCurrencySelected != null && receiveCurrencySelected != null) {
            viewModelScope.launch {
                _receiveMoney.value =
                    calculateNewBalanceUseCase(
                        CalculateNewBalanceUseCase.Params(
                            sellCurrencySelected.rate,
                            receiveCurrencySelected.rate,
                            text?.toDoubleOrNull() ?: 0.0
                        )
                    )
            }
        }
    }

    fun covertCurrency(
        amount: String?,
        sellCurrencySelected: CurrencyRateView?,
        receiveCurrencySelected: CurrencyRateView?
    ) {
        if (sellCurrencySelected != null && receiveCurrencySelected != null) {
            viewModelScope.launch {
                _covertCurrency.value = covertCurrencyUseCase(
                    CovertCurrencyUseCase.Params(
                        currencyRateViewToCurrencyRate.map(sellCurrencySelected),
                        currencyRateViewToCurrencyRate.map(receiveCurrencySelected),
                        amount?.toDoubleOrNull() ?: 0.0
                    )
                )
            }
        }
    }

    @AssistedInject.Factory
    interface Factory : AssistedSavedStateViewModelFactory<CurrencyConverterViewModel> {
        override fun create(savedStateHandle: SavedStateHandle): CurrencyConverterViewModel
    }
}
