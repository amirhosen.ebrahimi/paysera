package com.paySera.supperApp.placeList.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.paySera.supperApp.designSystem.databinding.BalanceItemBinding
import com.paySera.supperApp.placeList.presentation.entity.BalanceView

internal class BalanceListAdapter :
    ListAdapter<BalanceView, PlaceViewHolder>(
        PLACE_COMPARATOR
    ) {


    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        return PlaceViewHolder(
            BalanceItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    companion object {
        private val PLACE_COMPARATOR = object : DiffUtil.ItemCallback<BalanceView>() {
            override fun areItemsTheSame(oldItem: BalanceView, newItem: BalanceView): Boolean =
                oldItem.name == newItem.name && oldItem.amount == newItem.amount


            override fun areContentsTheSame(oldItem: BalanceView, newItem: BalanceView): Boolean =
                oldItem.name == newItem.name && oldItem.amount == newItem.amount
        }
    }
}

internal class PlaceViewHolder(
    private val binding: BalanceItemBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(balance: BalanceView?) { // todo
        binding.nameAndAmount.text = balance?.amount.toString() + "   " + balance?.name
    }
}



