package com.paySera.supperApp.placeList.presentation.entity

data class CurrencyRateView(
    val name: String,
    val rate: Double,
    val isBase : Boolean
)

