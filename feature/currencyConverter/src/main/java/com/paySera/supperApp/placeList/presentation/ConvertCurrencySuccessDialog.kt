package com.paySera.supperApp.placeList.presentation

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.paySera.supperApp.common.sdkextentions.properties.viewBinding
import com.paySera.supperApp.placeList.R
import com.paySera.supperApp.placeList.databinding.DialogConvertCurrencySuccessBinding

class ConvertCurrencySuccessDialog : DialogFragment(R.layout.dialog_convert_currency_success) {
    val args: ConvertCurrencySuccessDialogArgs by navArgs()
    private val binding by viewBinding(DialogConvertCurrencySuccessBinding::bind)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.title.text = args.title
        binding.submitBtn.setOnClickListener {
            dismiss()
        }
    }
}