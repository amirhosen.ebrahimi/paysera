package com.paySera.supperApp.placeList.presentation.di

import com.paySera.supperApp.placeList.PlaceListScope
import com.paySera.supperApp.placeList.presentation.CurrencyConverterViewModel
import com.paySera.supperApp.common.di.AssistedSavedStateViewModelFactory
import com.paySera.supperApp.common.di.ViewModelKey
import androidx.lifecycle.ViewModel
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@AssistedModule
@Module(includes = [AssistedInject_PlaceListAssistedModule::class])
abstract class PlaceListAssistedModule {
    @Binds
    @IntoMap
    @PlaceListScope
    @ViewModelKey(CurrencyConverterViewModel::class)
    abstract fun bindVMFactory(f: CurrencyConverterViewModel.Factory): AssistedSavedStateViewModelFactory<out ViewModel>

}