package com.paySera.supperApp.placeList.presentation

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.paySera.supperApp.common.di.ViewModelFactory
import com.paySera.supperApp.common.sdkextentions.properties.viewBinding
import com.paySera.supperApp.placeList.R
import com.paySera.supperApp.placeList.databinding.FragmentCurrencyConverterBinding
import dagger.android.support.DaggerFragment
import kotlinx.coroutines.flow.*
import javax.inject.Inject
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.paySera.supperApp.common.onError
import com.paySera.supperApp.common.onSuccess
import com.paySera.supperApp.common.sdkextentions.properties.autoCleared
import com.paySera.supperApp.domain.usecase.CovertCurrencyUseCase
import com.paySera.supperApp.placeList.presentation.adapter.BalanceListAdapter
import com.paySera.supperApp.tasks.updateCurrencyRates.presentation.UpdateCurrencyRatesWorker
import kotlinx.coroutines.delay

class CurrencyConverterFragment : DaggerFragment(R.layout.fragment_currency_converter),
    OnItemSelectedListener {
    @Inject
    lateinit var abstractFactory: ViewModelFactory

    private val viewModel by viewModels<CurrencyConverterViewModel> {
        abstractFactory.create(
            this,
            arguments
        )
    }

    private val binding by viewBinding(FragmentCurrencyConverterBinding::bind)
    private var adapter by autoCleared<BalanceListAdapter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = BalanceListAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.listMyBalances.adapter = adapter
        binding.listMyBalances.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.HORIZONTAL, false)

        WorkManager.getInstance(requireContext()).enqueue(
            OneTimeWorkRequestBuilder<UpdateCurrencyRatesWorker>().build()
        )

        viewModel.getCurrencyRates()
        viewModel.getAllBalances()
        lifecycleScope.launchWhenStarted {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.currencyRates.collectLatest {
                    it?.onSuccess {
                        Log.i("amir91", "asas")
                        binding.sellCurrencySpinner.adapter = createAdapter(it.map { it.name })
                        binding.receiveCurrencySpinner.adapter = createAdapter(it.map { it.name })
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.receiveMoney.collectLatest {
                    it.onSuccess {
                        binding.editTextReceive.setText(it.toString())
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.allBalances.collectLatest {
                    it.onSuccess {
                        adapter.submitList(it)
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewLifecycleOwner.lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.covertCurrency.collectLatest {
                    it?.onSuccess {
                        findNavController().navigate(
                            CurrencyConverterFragmentDirections.actionCurrencyConverterFragmentToConvertCurrencySuccessDialog(
                                getString(
                                    R.string.title_success_currency_covert, it.sellBalances.name,
                                    it.sellBalances.amount.toString(),
                                    it.receiveBalances.name,
                                    it.receiveBalances.amount.toString(),
                                    CovertCurrencyUseCase.commissionFreeCurrency,
                                    it.commissionFreeCurrencyAmount.toString()
                                )
                            )
                            /*CurrencyConverterFragmentDirections.actionCurrencyConverterFragmentToConvertCurrencySuccessDialog(
                                it.sellBalances.name,
                                it.sellBalances.amount.toString(),
                                it.receiveBalances.name,
                                it.receiveBalances.amount.toString(),
                                CovertCurrencyUseCase.commissionFreeCurrency,
                                it.commissionFreeCurrencyAmount.toString()
                            )*/
                        )
                    }
                    it?.onError { error, data ->
                        findNavController().navigate(
                            CurrencyConverterFragmentDirections.actionCurrencyConverterFragmentToConvertCurrencySuccessDialog(
                                error.message ?: ""
                            )
                        )
                    }
                }
            }
        }
        binding.editTextSellCurrency.doOnTextChanged { text, start, before, count ->
            viewModel.calculateNewBalance(
                text.toString(),
                viewModel.getCurrencySelected(binding.receiveCurrencySpinner.selectedItemPosition),
                viewModel.getCurrencySelected(binding.sellCurrencySpinner.selectedItemPosition)
            )
        }
        binding.submitBtn.setOnClickListener {
            viewModel.covertCurrency(
                binding.editTextSellCurrency.text.toString(),
                viewModel.getCurrencySelected(binding.sellCurrencySpinner.selectedItemPosition),
                viewModel.getCurrencySelected(binding.receiveCurrencySpinner.selectedItemPosition)
            )
        }
        binding.receiveCurrencySpinner.onItemSelectedListener = this
        binding.sellCurrencySpinner.onItemSelectedListener = this
    }

    private fun createAdapter(list: List<String>): ArrayAdapter<String> {
        val adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            list
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        return adapter
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        viewModel.calculateNewBalance(
            binding.editTextSellCurrency.text.toString(),
            viewModel.getCurrencySelected(binding.receiveCurrencySpinner.selectedItemPosition),
            viewModel.getCurrencySelected(binding.sellCurrencySpinner.selectedItemPosition)
        )
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }


}