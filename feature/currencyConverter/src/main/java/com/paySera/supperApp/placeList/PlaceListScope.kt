package com.paySera.supperApp.placeList

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class PlaceListScope