package com.paySera.supperApp.placeList.presentation.mapper

import com.paySera.supperApp.common.mapper.Mapper
import com.paySera.supperApp.domain.entity.Balances
import com.paySera.supperApp.placeList.presentation.entity.BalanceView
import javax.inject.Inject

class BalanceListToBalanceViewList @Inject constructor() :
    Mapper< @JvmSuppressWildcards List<Balances>,@JvmSuppressWildcards List<BalanceView>> {
    override fun map(first: List<Balances>): List<BalanceView> {
        return first.map {
            BalanceView(it.name, it.amount)
        }
    }
}